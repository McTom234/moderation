package database

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var db *gorm.DB

func init() {
	var err error
	db, err = gorm.Open(sqlite.Open("moderation.db"), &gorm.Config{})
	if err != nil {
		panic("Failed to open database with error: " + err.Error())
	}
	err = db.AutoMigrate(Action{}, User{})
	if err != nil {
		panic("Failed to migrate tables with error: " + err.Error())
	}
}
