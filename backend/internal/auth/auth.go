package auth

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/jwtauth/v5"
	"github.com/go-ini/ini"
	"github.com/lestrrat-go/jwx/jwa"
	"github.com/lestrrat-go/jwx/jwt"

	"codeberg.org/codeberg/moderation/internal/log"
)

var (
	JwtAuth  *jwtauth.JWTAuth
	jwtAlg   jwa.SignatureAlgorithm = jwa.HS256
	userMap  map[string]string
	giteaURL string
)

type PermissionLevel int

const (
	PERMISSION_USER   PermissionLevel = iota + 1 // nolint:deadcode
	PERMISSION_MEMBER                            // nolint:deadcode
	PERMISSION_MODERATOR
	PERMISSION_ADMIN
)

type Moderator struct {
	Login string
	Level PermissionLevel
}

func Config(cfg *ini.File) {
	giteaURL = cfg.Section("gitea").Key("URL").String()

	jwtSecret := cfg.Section("security").Key("JWT_SECRET").String()
	if jwtSecret == "" || jwtSecret == "default_jwt_secret" {
		log.Fatal("Please make sure you set a unique jwt secret in the moderation_backend.ini")
	}

	userMap = make(map[string]string)
	for _, user := range cfg.Section("security.users").Keys() {
		userMap[user.Name()] = user.String()
	}
	if len(userMap) == 0 {
		log.Warn("You should add at least one user under the [user] section in moderation_backend.ini")
	}

	JwtAuth = jwtauth.New(string(jwtAlg), []byte(jwtSecret), nil)
}

func Basicauth(r chi.Router) {
	r.Use(middleware.BasicAuth("user", userMap))
	r.Get("/auth/login", login)
}

func login(w http.ResponseWriter, r *http.Request) {
	expiration := time.Now().Add(3 * 24 * time.Hour)
	_, newToken, _ := JwtAuth.Encode(map[string]interface{}{"p": PERMISSION_ADMIN, "login": "BASICAUTH", "exp": expiration.UTC().Unix()})
	cookie := http.Cookie{Name: "jwt", Value: newToken, Expires: expiration, HttpOnly: true, Secure: true, Path: "/"}
	http.SetCookie(w, &cookie)
	w.Header().Set("Location", "/")
	w.WriteHeader(http.StatusSeeOther)
	_, _ = w.Write([]byte(newToken))
}

func Require(r *http.Request, requiredPermissionLevel PermissionLevel) (Moderator, error) {
	token, err := VerifyRequest(r)
	if err != nil {
		return Moderator{}, log.NewError("jwtauth.FromContext", err)
	}

	permission, exists := token.Get("p")

	if !exists {
		return Moderator{}, log.NewError("Authenticate: No token")
	}

	login, exists := token.Get("login")
	if !exists {
		return Moderator{}, log.NewError("Authenticate: No login name")
	}

	moderator := Moderator{
		Level: PermissionLevel(permission.(float64)),
		Login: login.(string),
	}

	if moderator.Level < requiredPermissionLevel {
		return moderator, log.NewError("Authenticate: " + fmt.Sprintf("Authentication level of %d does not match required level of %d", moderator.Level, requiredPermissionLevel))
	}
	return moderator, nil
}

func VerifyRequest(r *http.Request) (jwt.Token, error) {
	tokenString := jwtauth.TokenFromCookie(r)
	if tokenString == "" {
		tokenString = jwtauth.TokenFromHeader(r)
	}
	return jwtauth.VerifyToken(JwtAuth, tokenString)
}
