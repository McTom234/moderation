package mailer

import (
	"bytes"
	"fmt"
	"text/template"

	"code.gitea.io/sdk/gitea"
	"github.com/go-ini/ini"
	"gopkg.in/gomail.v2"
)

var (
	mailDialer    *gomail.Dialer
	mailFromname  string
	mailAlwaysBcc string
	mailGiteaURL  string
	mailGiteaName string
)

func Config(cfg *ini.File) {
	mailconfig := cfg.Section("mail")
	hostname := mailconfig.Key("HOSTNAME").String()
	port := mailconfig.Key("PORT").MustInt()
	username := mailconfig.Key("USERNAME").String()
	password := mailconfig.Key("PASSWORD").String()
	mailFromname = mailconfig.Key("FROMNAME").String()
	mailAlwaysBcc = mailconfig.Key("BCC").String()
	mailDialer = gomail.NewDialer(hostname, port, username, password)

	giteaConfig := cfg.Section("gitea")
	mailGiteaURL = giteaConfig.Key("URL").String()
	mailGiteaName = giteaConfig.Key("NAME").String()
}

// findOwnerTeam loops over the teams and return the "Owner" team.
// If none is found, return nil.
func findOwnerTeam(teams []*gitea.Team) *gitea.Team {
	for _, team := range teams {
		if team.Name == "Owner" {
			return team
		}
	}
	return nil
}

// MailSend will send a mail to the given user. The function takes in a template and template data.
// If the user is a organization, it will instead send mails to the owners of the organization.
func MailSend(giteaClient *gitea.Client, user *gitea.User, templateName string, templateData map[string]interface{}) error {
	// Check if the user is a organization.
	_, _, err := giteaClient.GetOrg(user.UserName)

	// If the API returns no error,
	isOrg := err == nil

	// If the user isn't a organization and the mail is empty, return an error.
	if !isOrg {
		if user.Email == "" {
			return fmt.Errorf("user %q, has no mail", user.UserName)
		}

		return mailSend(user.Email, user.UserName, templateName, templateData)
	}

	// If user is organization, fetch all owners and send them an individual mail.
	teams, _, err := giteaClient.SearchOrgTeams(user.UserName, &gitea.SearchTeamsOptions{
		Query: "Owner",
	})
	if err != nil {
		return fmt.Errorf("SearchOrgTeams: %v", err)
	}

	// Find the owner team.
	ownerTeam := findOwnerTeam(teams)
	if ownerTeam == nil {
		return fmt.Errorf("couldn't find owner team of organisation %q", user.UserName)
	}

	// List all the members of the owner team.
	users, _, err := giteaClient.ListTeamMembers(ownerTeam.ID, gitea.ListTeamMembersOptions{})
	if err != nil {
		return fmt.Errorf("ListTeamMembers: %v", err)
	}

	// Send each member of the owner team a mail.
	// Don't immediately fail when mailsend returns a error.
	// Instead save the error "for later".
	for _, user := range users {
		if user.Email == "" {
			continue
		}

		mailErr := mailSend(user.Email, user.UserName, templateName, templateData)
		if mailErr != nil {
			err = mailErr
		}
	}

	return err
}

func mailSend(mailAdress, userName, templateName string, templateData map[string]interface{}) error {
	// Add some extra data to the template.
	templateData["toName"] = userName
	templateData["giteaURL"] = mailGiteaURL
	templateData["giteaName"] = mailGiteaName

	// Parse the template.
	mailTemplate, err := template.ParseFiles("templates/mail/" + templateName + ".tmpl")
	if err != nil {
		return fmt.Errorf("ParseFiles[template=%s]: %v", templateName, err)
	}

	// Actually execute the template with the provided data and save it into mailContent.
	var mailContent bytes.Buffer
	if err := mailTemplate.Execute(&mailContent, templateData); err != nil {
		return fmt.Errorf("error parsing mail template: %v", err)
	}

	mail := gomail.NewMessage()
	mail.SetHeader("From", mailFromname)
	mail.SetHeader("To", mailAdress)
	if mailAlwaysBcc != "" {
		mail.SetHeader("Bcc", mailAlwaysBcc)
	}
	mail.SetHeader("Subject", "Important notice regarding your content on Codeberg.org")
	mail.SetBody("text/plain", mailContent.String())

	if err = mailDialer.DialAndSend(mail); err != nil {
		return fmt.Errorf("error sending email: %v", err)
	}

	return nil
}
