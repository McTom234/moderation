.PHONY: dev run install fmt frontend
dev: run fmt
run: install frontend
	cd backend && go run .
install: backend/go.sum
	cd backend && go get
fmt:
	cd backend && go fmt && gofumpt -extra -d .
frontend:
	cd frontend && npm install && npm run build
