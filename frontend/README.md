# Codeberg Moderation Tool Frontend

## Definitions

### Input action buttons definition

The input action buttons definition can be found in [./ts/inputDefinition.ts](./ts/inputDefinition.ts).

The static input buttons will always be display and not inside a tab. All other buttons will be rendered in tabs.

The `Tab` type contains the tab title and the buttons for this tab. The `Buttons` type describe a button where the key
is the label and the value the onclick function.

### List action buttons definition

THe list action buttons definition can be found in [./ts/actionDefinition.ts](./ts/actionDefinition.ts).

The action tabs are defined in arrays where the key is the input type of the list (in this context the last list in
the `lists` array).

The `Tab` objects contains the title and an array with the input definitions. The `Action` type describes an input.
Following attributes must be assigned:

* `type`: the input type
* `name`: the name of the button
* `label`: the content of the label for the button

Furthermore, an onclick behaviour can be defined with the `onclick` attribute and any other attributes with
the `attributes` Map object. The easiest way to define the map is by creating an object the same way the `Buttons`
in [Input action buttons definition](#Input-action-buttons-definition) would be created and then passing it to the
function `objectToMap`.
