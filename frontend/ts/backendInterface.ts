import { authJWT } from './auth';
import { List } from './main';

type RequestProperties = {
	method: string
	headers: HeadersInit
	needsConfirmation?: boolean
	inputType?: string
}

/**
 * This function sends a request to the backend.
 * @param action Url to the backend server.
 * @param callback Function that is called after the response is fetched.
 * @param list List input for post requests.
 * @param properties RequestProperties object.
 * @param inputsIds Array of ids in DOM that values should be fetched and send with the request.
 */
export function callBackend (action: string, callback: (data: any) => void, list?: List, properties: RequestProperties = {method: 'GET', headers: {}}, inputsIds: string[] = []): void {
	// request is the body of the request
	let request: Map<string, any> = new Map<string, any>();
	let requestProperties: RequestInit = {};

	// fetch inputs and map them to the request
	for (let inputId of inputsIds) {
		const input = document.getElementById(inputId) as HTMLInputElement;
		if (input === null) continue;

		inputId = inputId.replace('cb-action-input-', '');

		if (input.getAttribute('type') === 'checkbox')
			request.set(inputId, input.checked);
		else
			request.set(inputId, input.value);
	}

	// check if confirmation is needed
	if (properties.needsConfirmation && !confirm('Do you really want to call ' + action + '?')) {
		// abort if confirmation fails
		alert('Backend request aborted. You need to confirm this one!');
		return;
	}

	// set headers
	requestProperties.headers = Object.assign({
		'Authorization': 'BEARER ' + authJWT
	}, properties.headers);

	// map list to request
	if (list !== undefined && list.type !== undefined)
			if ((properties.inputType && list.type == properties.inputType) || !properties.inputType)
				request.set('list', listRowsMapToArray(list.listRows));

	// set method
	if (properties.method) {
		const method = properties.method;
		requestProperties.method = method;
		if (method!.toLowerCase() !== 'head' && method!.toLowerCase() !== 'get')
			// encode request body
			requestProperties.body = JSON.stringify(Object.fromEntries(request));
	}

	// start the request
	fetch(action, requestProperties).then(response => response.json(), console.error).then(data => {
		// call callback
		callback(data);
	});
}

/**
 * This function converts list rows to an object array
 * @param list
 */
function listRowsMapToArray (list: Map<string, any>[]) {
	const res: object[] = [];
	list.forEach((value) => res.push(Object.fromEntries(value)));
	return res;
}
