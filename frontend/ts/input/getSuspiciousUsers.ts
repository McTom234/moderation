import { callBackend } from '../backendInterface';
import { addToGeneratedBy, createList } from '../list';
import { lists } from '../main';
import { render, renderAll } from '../render';

/**
 * This function is an alias using the `callBackend` function
 */
export function getSuspiciousUsers () {
	callBackend('/users/list/suspicious', (data) => {
		// transfer data into a map
		const dataAsMap: Map<string, any>[] = [];
		for (const dataObject of data) {
			dataAsMap.push(new Map(Object.entries(dataObject)));
		}

		// create a new list and add data to it
		createList(0);
		createList(1, 'Ignore');
		createList(2, 'Remove');
		createList(3, 'Mark-as-legit');
		renderAll();
		addToGeneratedBy(0, 'getSuspiciousUsers()');
		addToGeneratedBy(1, 'getSuspiciousUsers()');
		addToGeneratedBy(2, 'getSuspiciousUsers()');
		addToGeneratedBy(3, 'getSuspiciousUsers()');
		lists[0].type = 'user';
		lists[0].listRows = dataAsMap;
		// render the list
		render(0);
	}, undefined, {
		method: 'GET',
		headers: {}
	});
}
