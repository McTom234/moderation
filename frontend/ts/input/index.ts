// file to import from `./input`

import { addToGeneratedBy, createList } from '../list';
import { lists } from '../main';
import { render } from '../render';

export * from './buttons';
export * from './import';
export * from './textField';

/**
 * This function processes data and add it to the first list (index 0).
 * @param data String containing new data.
 */
export function processInput (data: string) {
	// replace links in the given string and split URLs into array
	const inputArr = data.replace(/^(https:\/\/[^/]*|)\//gm, '').split('\n').filter((line: string) => line !== '')
		.map((url: string) => url.split('/'));

	// create list if necessary
	if (lists.length === 0)
		createList();

	// set the generatedBy tag of the list
	addToGeneratedBy(0, 'processing text input');

	// set input type if list is empty
	if (lists[0].listRows.length === 0) {
		lists[0].type = getInputType(inputArr);
		lists[0].listRows = [];
	}

	// map data to map according to list type
	let newInput: Map<string, any>[];

	switch (lists[0].type) {
		case 'user':
			newInput = inputArr.map((fields: string[]) => new Map<string, any>(Object.entries({
				login: fields[0]
			})));
			break;
		case 'repo':
			newInput = inputArr.map((fields: string[]) => new Map<string, any>(Object.entries({
				owner: fields[0],
				name: fields[1]
			})));
			break;
		default:
			newInput = [];
	}

	// ada new data map to list rows
	lists[0].listRows = lists[0].listRows.concat(newInput);
	// render the list
	render(0);
}


/**
 * This function determines which type an input is (e.g. users or repos or issues etc.).
 * @param inputArr Input data as string.
 * @returns {string|undefined} Type of list input.
 */
export function getInputType (inputArr: string[][]) {
	// remove empty rows
	while (inputArr.length > 0 && (inputArr[0].length === 0 || inputArr[0][0] === ''))
		inputArr.shift();

	// no data is present if length is 0 -> type is undefined
	if (inputArr.length <= 0) return undefined;

	// determine type based on length
	switch (inputArr[0].length) {
		case 1:
			return 'user';
		case 2:
			return 'repo';
		default:
			return undefined;
	}
}
