import { addToGeneratedBy, createList } from '../list';
import { lists } from '../main';
import { render } from '../render';
import { getInputType, processInput } from './index';

/**
 * This function gets files from input.
 * @param onload Function that will be called when file was read.
 */
function loadFromFile (onload: (event: ProgressEvent<FileReader>) => void) {
	// open file dialog
	document.getElementById('cb-upload-list')!.click();
	// when files were changed
	document.getElementById('cb-upload-list')!.onchange = () => {
		// get the file list
		const files = (document.getElementById('cb-upload-list') as HTMLInputElement).files;
		if (files === null) return;
		// iterate through files
		for (let fileIndex = 0; fileIndex < files.length; fileIndex++) {
			const reader = new FileReader();
			reader.onload = onload;
			reader.onerror = (e) => alert('error: ' + e.target?.error?.name);
			reader.readAsText(files.item(fileIndex)!);
		}
	};
}

/**
 * This function loads data from a text file.
 */
function loadFromTextFile () {
	loadFromFile((e) => {
		// create a new list
		createList(0);
		// add generatedBy tag
		addToGeneratedBy(0, 'reading text-file()');
		// process the input
		processInput((e.target?.result as string).split(/\r\n|\n/).join('\n'));
	});
}

/**
 * This function imports data from a CSV file.
 */
function loadFromImportFile () {
	loadFromFile((e) => {
		// get lines
		const lines: string[] = (e.target!.result as string).split(/\r\b|\n/);
		// get the column header names from the first row
		const columnNames: string[] = lines.shift()!.split(';');

		// iterate through lines and store rows as maps
		const data: Map<string, any>[] = [];
		for (const line of lines) {
			const element: Map<string, any> = new Map<string, any>();
			const fields = line.split(';');
			if (fields.length == 0 || fields.length == 1 && fields[0] == '') continue;

			for (let i = 0; i < columnNames.length; i++) {
				if (columnNames[i] == undefined || columnNames[i] == '') continue;
				element.set(columnNames[i], fields[i]);
			}
			data.push(element);
		}

		// create new list and set generatedBy tag
		createList(0);
		addToGeneratedBy(0, 'file import');

		// set type and data
		lists[0].type = getInputType(data.map<string[]>((map) => {
			const a: string[] = [];
			map.forEach((value) => {
				a.push(value);
			});
			return a;
		}));
		lists[0].listRows = data;

		// render list
		render(0);
	});
}


// ASSIGN FUNCTIONS TO ELEMENTS

document.getElementById('cb-input-load-button')!.addEventListener('click', () => loadFromTextFile());

document.getElementById('cb-input-import-button')!.addEventListener('click', () => loadFromImportFile());
