import { lists } from '../main';
import { render } from '../render';
import { addToGeneratedBy, createList, dropList, findListByTitle } from './list';

/**
 * THis function copies a row to the next list. If the next list has not the same type a new one is inserted
 * @param source Index of the list of the target row.
 * @param row Index of the row in the list with index given in source.
 */
export function copyRow (source: number, row: number, to?: string) {
	let copyTo = to ? findListByTitle(to) : source + 1;
	if (copyTo === undefined) copyTo = source + 1;

	// create new list if there is no next list
	if (lists[copyTo] === undefined) {
		createList();
		lists[copyTo].type = lists[source].type;
	}

	// copy row
	lists[copyTo].listRows = lists[copyTo].listRows.concat(lists[source].listRows[row]);
	// add generatedBy tag
	addToGeneratedBy(copyTo, 'copy');
	// render the list
	render(copyTo);
}

/**
 * This function removes a single row from a list.
 * @param index Index of the list in the lists array.
 * @param row Index of the row in the list.
 */
export function dropRow (index: number, row: number) {
	// remove row
	lists[index].listRows.splice(row, 1);
	// add generatedBy tag
	addToGeneratedBy(index, 'drop');
	// drop list if list is empty
	if (lists[index].listRows.length === 0) dropList(index);
	// render the list otherwise
	else render(index);
}
