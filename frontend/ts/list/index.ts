// file to import from `./input`

import { lists } from '../main';

export * from './export';
export * from './list';
export * from './row';

/**
 * This function gets the column names of a list.
 * @param index Index of the list.
 */
export function getColumnNames (index: number) {
	const columns: string[] = [];
	for (const listRow of lists[index].listRows) {
		listRow.forEach((_, key) => {
			if (columns.indexOf(key) == -1) columns.push(key);
		});
	}
	return columns;
}
