import { getSuspiciousUsers } from './input/getSuspiciousUsers';

export type Buttons = {
	[text: string]: () => void
};

export type Tab = {
	title: string
	buttons: Buttons
}

/**
 * This definition generates static buttons like the Import-Buttons over the input button tab container.
 * TODO - necessary? remove?
 */
export const staticInputButtons: Buttons = {};

/**
 * Definition of the input tabs and input buttons.
 */
export const inputTabs: Tab[] = [
	{
		title: 'Users',
		buttons: {
			'Find suspicious users': getSuspiciousUsers,
			'Delete user': () => console.log('delete user')
		}
	},
	{
		title: 'Repos',
		buttons: {
			'Find large repos': () => {}
		}
	}
];
