import '../css/index.css';

export type List = {
	title?: string
	type?: string
	listRows: Map<string, any>[]
	renderTarget: HTMLElement
	generatedBy: string[]
}

export const lists: List[] = [];

import './auth';
import './backendInterface';
import './input';
import './list';
import './render';
import { auth } from './auth';

auth();
setTimeout(auth, 270000);
