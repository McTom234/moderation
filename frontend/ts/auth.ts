export let authJWT: string;

/**
 * This function authenticates the user.
 */
export function auth () {
	fetch('/auth/login').then(response => response.text())
		.then(text => {
			authJWT = text;

			// request to fetch the user from JWT
			fetch('/user', {
				headers: {
					'Authorization': 'BEARER ' + authJWT
				}
			}).then(response => {
				// if response status is 401, user is not logged in. Log that!
				if (response.status == 401) {
					console.warn('not logged in');
				} else
					// display username on deactivated button
					response.text().then(data => {
						let oauthLoginLink = (document.getElementById('cb-auth-login') as HTMLLinkElement);
						oauthLoginLink.innerHTML = data;
						oauthLoginLink.classList.add('deactivated');
					});
			});
		});
}
