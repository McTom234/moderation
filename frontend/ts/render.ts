import { generateTabs as generateActionTabs } from './action';
import { getInputType } from './input';
import { askForListName, clearList, copyList, copyRow, dropList, dropRow, exportList, getColumnNames } from './list';
import { lists } from './main';

export function renderAll () {
	for (const index in lists) {
		console.log(lists[index]);
		render(Number(index));
	}
}

/**
 * This function renders a table based on the lists array object
 * @param index Index of the list in the lists array to be rendered.
 */
export function render (index: number) {
	// get column names
	const columns: string[] = getColumnNames(index);

	/**
	 * This function creates a button.
	 * @param content Text content of the button.
	 * @param styles Additional styles to add to the button.
	 */
	function abstractButton (content: string, ...styles: string[]) {
		const abstractButton = document.createElement('button');
		abstractButton.innerHTML = content;
		abstractButton.classList.add('ml-5', 'btn', 'mb-5', ...styles);
		return abstractButton;
	}


	// create table head with column names
	const tableHead = document.createElement('thead');
	tableHead.innerHTML = `
		<tr class="position-relative">
			<th class="position-sticky left-0">Actions</th>
			${ columns.map(columnName => `<th>${ columnName }</th>`).join('') }
		</tr>
	`;


	// create table body
	const rowButtons: { [b: string]: { onClick: (row: number) => () => void, styles: string[] } } = {
		'&#x2715;': {
			onClick: (row: number) => () => dropRow(index, row),
			styles: ['btn-danger']
		},
		'&rarr;': {
			onClick: (row: number) => () => copyRow(index, row),
			styles: ['btn-primary']
		},
		'&plus;': {
			onClick: (row: number) => () => {
				askForListName(index);
				copyRow(index, row);
			},
			styles: ['btn-primary']
		}
	};
	const tableBody = document.createElement('tbody');
	lists[index].listRows.forEach((rowData, rowIndex) => {
		// generate row buttons
		const result: HTMLButtonElement[] = [];
		for (const inputButtonLabel in rowButtons) {
			const button = abstractButton(inputButtonLabel, ...rowButtons[inputButtonLabel].styles);
			button.addEventListener('click', rowButtons[inputButtonLabel].onClick(rowIndex));
			result.push(button);
		}
		const listButtons: HTMLButtonElement[] = [];
		for (const list of lists) {
			if (list.title === undefined) continue;
			const button = abstractButton(list.title, 'btn-secondary');
			button.addEventListener('click', () => {
				copyRow(index, rowIndex, list.title);
			});
			listButtons.push(button);
		}

		// <td> for row buttons
		const td = document.createElement('td');
		td.classList.add('position-sticky', 'left-0');
		td.append(...result, listButtons.length > 0 ? document.createElement('br') : '', ...listButtons);

		// render columns of list and prepend row buttons
		const tr = document.createElement('tr');
		tr.classList.add('position-relative');
		tr.innerHTML = columns.map(columnName => `<td>${ renderTableFormatCell(columnName, rowData.get(columnName)) }</td>`)
			.join('');
		tr.prepend(td);

		tableBody.append(tr);
	});


	// create table foot
	const td = document.createElement('td');
	td.colSpan = columns.length + 1;
	td.classList.add('text-center');

	const tr = document.createElement('tr');
	tr.append(td);

	const tableFoot = document.createElement('tfoot');
	tableFoot.append(tr);


	// create list action buttons
	const listButtons: { [b: string]: { onClick: () => void, styles: string[] } } = {
		'EXPORT LIST TO CSV FILE': {
			onClick: () => exportList(index),
			styles: ['btn-primary']
		},
		'COPY TO NEXT LIST': {
			onClick: () => copyList(index),
			styles: ['btn-primary']
		},
		'COPY TO NEW LIST': {
			onClick: () => {
				askForListName(index);
				copyList(index);
			},
			styles: ['btn-primary']
		},
		'CLEAR LIST': {
			onClick: () => clearList(index),
			styles: ['btn-primary']
		},
		'DROP LIST': {
			onClick: () => dropList(index),
			styles: ['btn-danger']
		}
	};
	// generate buttons
	const result: HTMLButtonElement[] = [];
	for (const inputButtonLabel in listButtons) {
		const button = abstractButton(inputButtonLabel, ...listButtons[inputButtonLabel].styles);
		button.addEventListener('click', listButtons[inputButtonLabel].onClick);
		result.push(button);
	}

	// create list action buttons container
	const listActionButtons = document.createElement('div');
	listActionButtons.classList.add('d-flex', 'justify-content-end', 'px-15', 'py-10', 'flex-wrap');
	listActionButtons.append(...result);


	// create table
	const table = document.createElement('table');
	table.classList.add('table', 'table-hover');
	table.append(tableHead, tableBody, tableFoot);

	// add a list heading with the type of the list and the generators of this list
	const listHeading = document.createElement('div');

	const typeString = lists[index].listRows.length < 1 ? undefined : getInputType(lists[index].listRows.map<string[]>((map) => {
		const a: string[] = [];
		map.forEach((value) => {
			a.push(value);
		});
		return a;
	}));
	const generatedByString = lists[index].generatedBy.join('\', \'');

	const listHeadingSubtitle = document.createElement('p');
	listHeadingSubtitle.innerHTML = `List type: '${ typeString }'<br>Generated by: '${ generatedByString }'`;

	let listHeadingTitle: string | HTMLHeadingElement = '';
	if (lists[index].title) {
		listHeadingTitle = document.createElement('h4');
		listHeadingTitle.innerHTML = lists[index].title!;
	}

	listHeading.append(listHeadingTitle, listHeadingSubtitle);

	// create parent div and append list heading and list table
	const parentDiv = document.createElement('div');
	parentDiv.classList.add('overflow-x-auto', 'overflow-y-hidden');
	parentDiv.append(table);

	// render table
	lists[index].renderTarget.replaceChildren(listHeading, parentDiv, listActionButtons);

	// regenerate the action buttons if this was the last list.
	if (index === lists.length - 1)
		generateActionTabs();
}

/**
 * THis function generates the td element content for the list.
 * @param column_name Name of the column.
 * @param content Content of the column.
 */
function renderTableFormatCell (column_name: string, content: any) {
	// determine how the data will be rendered
	switch (column_name) {
		case 'avatar_url':
			return '<img alt="" class="avatar" src="' + content + '">';
		case 'last_login':
		case 'created':
			const date = new Date(content);
			if (date.valueOf() == 0) {
				return 'never';
			}
			return '<div title="' + content + '">' + date.getFullYear() + '-' + date.getMonth() + '-' + date.getDay() + '</div>';
		default:
			return content;
	}
}
